package Logic;

import java.util.ArrayList;

public class ArticleManagement{
	
	/**
	 * A list of available articles ready to be borrowed
	 */
	private ArrayList<Article> articlesAvailable; 
	
	/**
	 * A list of articles that are already borrowed
	 */
	private ArrayList<Article> artilcesBorrowed; 
	
	public ArticleManagement(){
		articlesAvailable = new ArrayList<Article>();
		artilcesBorrowed = new ArrayList<Article>();
		
		//TODO: This article is only created for testing purposes temporarily:
		Article a = new Article("Lenovo 1",1);
		Article b = new Article("Lenovo 2",2);
		Article c = new Article("Lenovo 3",3);
		Article d = new Article("Lenovo 4",4);
		Article e = new Article("Lenovo 5",5);
		Article f = new Article("Lenovo 6",6);
		Article g = new Article("Lenovo 7",7);
		
		a.setDate("UNKNOWN");
		
		articlesAvailable.add(a);
		articlesAvailable.add(b);
		articlesAvailable.add(c);
		articlesAvailable.add(d);
		articlesAvailable.add(e);
		articlesAvailable.add(f);
		articlesAvailable.add(g);

		this.borrow(a,"Max");
		this.borrow(b,"Peter");
	}
	
	/**
	 * Add an Article to our "articlesAvailable" collection. All articles will first be available (articlesAvailable list) and can then be burrowed (articlesBorrowed), modified(?) and returned.
	 * @param name The name of the article to be added to our collection.
	 * @param value The value of the article to be added to our colletion.
	 */
	public void addArticleToCollection(String articleName, int articleID){
		Article a = new Article(articleName,articleID);
		articlesAvailable.add(a); //make the article available
		System.out.println("Article added to Collection.");
		
		for(Article b: articlesAvailable)
		System.out.println(b.getArticleName());
	}
	
	/**
	 * Borrow a Article from list articlesAvailable and add it to articlesBorrowed
	 * @param Article a the article to be borrowed
	 * @param username: WHO borrows the article?
	 */
	public void borrow(Article a,String username){

		if(a!=null && articlesAvailable.contains(a)){//If article a is available
			a.setName(username);
			a.setDate("UNKNOWN");//TODO:Add Date automatically here!
			this.artilcesBorrowed.add(a);//add article to borrowed
			this.articlesAvailable.remove(a);//remove article from being available
			System.out.println("Article borrowed from Collection.");
		}
	}
	
	public void returnArticle(Article a){
		if(a!=null && artilcesBorrowed.contains(a)){//If article is valid & borrowed
			this.articlesAvailable.add(a);//return it to available,
			this.artilcesBorrowed.remove(a);//...remove it from borrowed list...
		}
	}

	/**
	 * articlesAailable is a list containing all Articles that are available to be given away
	 * @return articlesAvailable 
	 */
	public ArrayList<Article> getArticlesAvailable() {
		return this.articlesAvailable; 
	}
	
	/**
	 * articlesBorrowed is a list containing all Articles that are borrowed.
	 * @return ArrayList articlesBorrowed
	 */
	public ArrayList<Article> getArticlesBorrowed() {
		return this.artilcesBorrowed; 
	}
	

}
