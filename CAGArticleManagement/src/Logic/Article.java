package Logic;

public class Article {
	
//"Article Name" "Article ID", "Verliehen an","Amounts Borrowed","Date"
	
	/**
	 * The name of the article borrowed
	 */
	private String articleName;
	
	/**
	 * the ID of the article borrowed
	 */
	private int articleID;
	
	/**
	 * The name of the person borrowing the article
	 */
	private String name;
	
	private String date;
	
	public Article(String articleName,int articleID){
		setArticleID(articleID);
		setArticleName(articleName);
	}


	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		if(articleName!=null)
			this.articleName = articleName;
	}

	public int getArticleID() {
		return articleID;
	}

	public void setArticleID(int articleID) {
		this.articleID = articleID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		if(date!=null)
			this.date = date;
	}

	/**
	 * the article that can be borrowed
	 */


	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name != null){
			this.name = name;
		}
	}
	
	

}