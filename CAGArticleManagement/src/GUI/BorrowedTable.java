package GUI;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Logic.Article;
import Logic.ArticleManagement;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BorrowedTable extends JPanel {
	
	private ArticleManagement aManagement;
	private GUI g;
	
	/**
	 * Creates a Table of all Articles that have been borrowed
	 * @param aManagement
	 */
    public BorrowedTable(ArticleManagement aManagement,GUI g) {
        super(new BorderLayout());
        if(g==null || aManagement == null){
        	throw new RuntimeException("GUI or articleManagment-Object can not be null!");
        }
        this.g=g;
        this.aManagement = aManagement;
        
        String[] columns = {"Article Name",
                                "Article ID", "Verliehen an",
                                "Date"};

        /**
         * The first dimension represents the data of one entire row
         * The second dimension each represents one data-segment
         */
      Object[][] data = new Object[aManagement.getArticlesBorrowed().size()][columns.length+aManagement.getArticlesBorrowed().size()]; 

      int index = 0;
      for(int i=0; i<data.length; i++){
    	 if(index<data[i].length){
    	  Article a = aManagement.getArticlesBorrowed().get(index);
    	  
       	  data[i][0]= a.getArticleName();
    	  data[i][1]= a.getArticleID();
    	  data[i][2]= a.getName();
    	  data[i][3]= a.getDate();
    	  
    	  index++;
    	 }
    	 else{
    		 break;
    	 }
      }

        JTable table = new JTable(data, columns);
        table.setPreferredScrollableViewportSize(new Dimension(1100, 400));
        table.setFillsViewportHeight(true);

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        
        //Add the scroll pane to this panel.
        this.add(scrollPane,BorderLayout.CENTER);
    }
}