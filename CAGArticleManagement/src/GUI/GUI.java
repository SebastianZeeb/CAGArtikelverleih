package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import Logic.ArticleManagement;

public class GUI extends JFrame implements ActionListener{
	
	
	/**
	 * The main JPanel
	 */
	private JPanel PMain; 
	
	private JPanel PSideL,PSideR;

	private JLabel labelLogo,labelLogo2;
	
	private JMenuItem menuItemBorrowed,menuItemAvailable,menuItemBorrowArticle,menuItemCreateArticle;
	
	private ArticleManagement aManagement;

	public GUI(){
		
		PMain = new JPanel();
		PSideL = new JPanel();
		PSideL.setBackground(Color.WHITE);
		PSideR = new JPanel();
		PSideR.setBackground(Color.WHITE);
		
		aManagement = new ArticleManagement();
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		
		labelLogo = new JLabel();
		labelLogo.setIcon(new ImageIcon("cagLogo.jpg"));
		PSideL.add(labelLogo);
		
		labelLogo2 = new JLabel();
		labelLogo2.setIcon(new ImageIcon("cagLogo.jpg"));
		PSideR.add(labelLogo2);
		
		menuItemBorrowed = new JMenuItem("Ausgeliehene Artikel");
		menuItemAvailable = new JMenuItem("Verf�gbare Artikel");
		menuItemBorrowArticle = new JMenuItem("Artikel Verleihen");
		menuItemCreateArticle = new JMenuItem("Artikel Hinzuf�gen");
		
		menuItemBorrowed.addActionListener(this);
		menuItemAvailable.addActionListener(this);
		menuItemBorrowArticle.addActionListener(this);
		menuItemCreateArticle.addActionListener(this);
		
		this.setLayout(new BorderLayout());
		PMain.add(new BorrowedTable(aManagement,this));
		this.add(PMain,BorderLayout.CENTER);
		this.add(PSideL,BorderLayout.WEST);
		this.add(PSideR,BorderLayout.EAST);
		
		menu.add(menuItemBorrowed);
		menu.add(menuItemAvailable);
		menu.add(menuItemBorrowArticle);
		menu.add(menuItemCreateArticle);
		
		menuBar.add(menu);
		this.setJMenuBar(menuBar);
	
		this.setTitle("Ausgeliehene Artikel");
		this.setVisible(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}


	/**
	 * Return the Main JPanel for external GUI modification
	 * @return
	 */
	public JPanel getPMain() {
		return PMain;
	}

	/**
	 * Sets the Main JPanel for the GUI
	 * @param pMain
	 */
	public void setPMain(JPanel pMain) {
		if(pMain != null){
			PMain = pMain;
		}
	}
	

	public static void main(String[] args) {
		GUI g = new GUI();
	

	}


	@Override
	public void actionPerformed(ActionEvent e) {
		//To recall previous resizing:
		int width = this.getWidth();
		int height = this.getHeight();
		
		if (e.getSource() == this.menuItemAvailable) {
			width = this.getWidth();
			height = this.getHeight();
			
			PMain.removeAll();
			PMain.add(new AvailableTable(aManagement,this));
			this.setTitle("Verf�gbare Artikel");
			this.pack();
			this.setSize(width,height);
			//this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		} else if (e.getSource() == this.menuItemBorrowed) {
			width = this.getWidth();
			height = this.getHeight();
			
			PMain.removeAll();
			PMain.add(new BorrowedTable(aManagement,this));
			this.setTitle("Ausgeliehene Artikel");
			this.pack();
			this.setSize(width,height);
		    //this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		} else if (e.getSource() == this.menuItemBorrowArticle) {
			System.out.println("Not implemented yet");
		} else if (e.getSource() == this.menuItemCreateArticle) {
			width = this.getWidth();
			height = this.getHeight();
			
			PMain.removeAll();
			PMain.add(new CreateArticle(aManagement,this));
			this.setTitle("Artikel Hinzuf�gen");
			this.pack();
			this.setSize(width,height);
		}
	}

}
