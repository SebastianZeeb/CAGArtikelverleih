package GUI;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Logic.Article;
import Logic.ArticleManagement;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AvailableTable extends JPanel {
	
	private ArticleManagement aManagement;
	private GUI g;
	
	/**
	 * Creates a Table of all available Articles that can be borrowed
	 */
    public AvailableTable(ArticleManagement aManagement, GUI g) {
        super(new BorderLayout());
        
        if(g==null || aManagement==null){
        	throw new RuntimeException("GUI- or ArticleManagement-Object can not be null!");
        }
        this.g=g;
        this.aManagement = aManagement;
        
        String[] columns = {"Article Name",
                                "Article ID"};

        /**
         * The first dimension represents the data of one entire row
         * The second dimension each represents one data-segment
         */
      Object[][] data = new Object[aManagement.getArticlesAvailable().size()][columns.length+aManagement.getArticlesAvailable().size()]; 

      for(Article b: aManagement.getArticlesAvailable()){
    	  System.out.println(b.getArticleName());
      }

      int index = 0;
      for(int i=0; i<data.length; i++){
    	 if(index<data[i].length){
    	  Article a = aManagement.getArticlesAvailable().get(index);

    	  data[i][0]= a.getArticleName();  
    	  data[i][1]= a.getArticleID();

    	  index++;
    	 }
    	 else{
    		 break;
    	 }
      }

        JTable table = new JTable(data, columns);
        table.setPreferredScrollableViewportSize(new Dimension(1100, 400));
        table.setFillsViewportHeight(true);

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        
        //Add the scroll pane to this panel.
        this.add(scrollPane,BorderLayout.CENTER);
    }

}