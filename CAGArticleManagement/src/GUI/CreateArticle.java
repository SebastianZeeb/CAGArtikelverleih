package GUI;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Logic.ArticleManagement;

public class CreateArticle extends JPanel implements ActionListener{
	
	private ArticleManagement aManagement;
	private GUI g;
	private JButton createButton; 
	private JLabel LName,LID;
	private JTextField TName,TID;
	
	private Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

	/**
	 * CreateArticle is used to put new articles into the collection. A new article is first put into the list of available articles.
	 * @param aManagement articleManagement Logic
	 * @param g GUI Frame
	 */
		public CreateArticle(ArticleManagement aManagement, GUI g){
			super.setLayout(new GridLayout(0,1));
			if(aManagement==null || g==null){
				throw new RuntimeException("articleManagment or GUI Object can not be null!");
			}
			this.aManagement=aManagement;
			this.g=g;
			
			LName = new JLabel("Artikelname:");
			LID = new JLabel("Artikel-ID:");
			
			TName = new JTextField();
			TID = new JTextField();
	
			createButton = new JButton("Artikel Erstellen");
			createButton.addActionListener(this);
			
			this.add(LName);
			this.add(TName);
			this.add(LID);
			this.add(TID);
			this.add(createButton);
			
			this.setSize(new Dimension(300,300));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(e.getSource()==createButton){ //Create the Article
				if(TName.getText()!=null && TID.getText()!=null){
					String articleName = TName.getText();
					int valueID = Integer.valueOf(TID.getText());
					aManagement.addArticleToCollection(articleName, valueID);
					
					int width = g.getWidth();
					int height = g.getHeight();
					
					g.getPMain().removeAll();
					g.getPMain().add(new AvailableTable(aManagement,g));
					g.setTitle("Verf�gbare Artikel");
					g.pack();
					g.setSize(width,height);

					}
			}
		}
		
		
}
